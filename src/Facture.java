import java.text.DateFormat;
import java.util.Calendar;
import java.text.SimpleDateFormat;
import java.util.Date;

public class Facture {
	

	public static void facture(Furniture f, Price p, Quantity q)
	{
		int factureNumber;
		factureNumber = 10000 + (int)(Math.random() * ((100000 - 10000) + 1));
		int selectedQuantity = 1 + (int)(Math.random() * ((q.getQuantity() - 1) + 1));
		
		DateFormat dateFormat = new SimpleDateFormat("yyyy/MM/dd");
		Date dateOfSale = new Date();
		Date dateDaysLeft = new Date();
		Calendar c = Calendar.getInstance(); 
		
		c.setTime(dateDaysLeft); 
		int daysLeft = 5 + (int)(Math.random() * ((100 - 5) + 1));
		c.add(Calendar.DATE, daysLeft);
		dateDaysLeft = c.getTime();
		
		System.out.println("Facture \n"+
							"Facture: " + factureNumber + "\n"+
							"Date of sale: "+dateFormat.format(dateOfSale)+"\n"+
							"Days left to pay: " + daysLeft +"\n"+
							"Days left to pay in date format yyy/mm/dd: " + dateFormat.format(dateDaysLeft)+"\n"+
							"Quantity: " + selectedQuantity+"\n"+
							"Total price net: " + p.getUnitNetPrice()*selectedQuantity +"$ \n"+
							"Total price gross: " + p.getUnitGrossPrice()*selectedQuantity +"$ \n"
							
				);
		
	}
};
