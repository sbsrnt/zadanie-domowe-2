public class Furniture implements Comparable<Furniture>{
	private String productName = "";
    private String color;
    private String woodType;
    private String clothType;

    public String getWoodType() {
		return woodType;
	}

	public void setWoodType(String woodType) {
		this.woodType = woodType;
	}

	public int compareTo(Furniture m) {
        return productName.compareTo(m.getProductName());
    }

    public void setProductName(String productName)
    {
        this.productName = productName;
    }
    
    public void setColor(String color){
    	this.color = color;
    }

    public String getProductName(){
        return productName;
    }
    
    public String getColor(){
    	return color;
    }

	public String getClothType() {
		return clothType;
	}

	public void setClothType(String clothType) {
		this.clothType = clothType;
	}
   
}